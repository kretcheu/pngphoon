/* tws.h - header file for libtws date/time library */


/* Definition of the tws data structure. */

#ifndef _TWS_H_
#define _TWS_H_ _TWS_H_

struct tws
{
   int     tw_sec;
   int     tw_min;
   int     tw_hour;
   
   int     tw_mday;
   int     tw_mon;
   int     tw_year;
   
   int     tw_wday;
   int     tw_yday;
   
   int     tw_zone;
   
   long    tw_clock;
   
   int     tw_flags;
#define TW_NULL 0x0000
#define TW_SDAY 0x0007        /* how day-of-week was determined */
#define TW_SEXP 0x0001        /*   explicitly given */
#define TW_DST  0x0010        /* daylight savings time */
};


/* Declarations of routines. */

struct tws *dlocaltime( );
        /* dlocaltime( &clock ) turns a time(3) clock value into a tws */

struct tws *dtwstime( );
        /* dtwstime( ) returns a tws for the current date/time */

#endif
