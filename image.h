
#ifndef _IMAGE_H_
#define _IMAGE_H_ _IMAGE_H_

#include <png.h>

struct image_s
{
   png_bytep bitmap;
   png_bytep *rowps;
   int       width;
   int       height;
   int       xbytes;
};

typedef struct image_s image_t;

image_t *imagecreate( int width, int height );
void imagedestroy( image_t *image );
void imageflip( image_t *image );

#endif
