
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include <png.h>
#include "fail.h"
#include "pngwrite.h"

void pngwrite( image_t *image, char* filename, int height, png_colorp palette_p )
{
   png_structp         png_ptr;
   png_infop           info_ptr;
   png_bytepp          firstrowps = image->rowps;
   volatile png_FILE_p fp = stdout;

   /* create file or use stdout */
   if( strcmp( filename, "-" ) )
   {
      /* create file */
      fp = fopen(filename, "wb");
   }

   if( !fp )
   {
      fail( "[pngwrite] File %s could not be opened for writing\n", filename );
   }


   /* initialize stuff */
   png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );

   if( !png_ptr )
   {
      fail( "[pngwrite] png_create_write_struct failed\n" );
   }

   info_ptr = png_create_info_struct(png_ptr);
   if( !info_ptr )
   {
      fail( "[pngwrite] png_create_info_struct failed\n" );
   }

#if USE_SETJMP
   if( setjmp(png_jmpbuf(png_ptr)) )
   {
      fail( "[pngwrite] Error during init_io\n" );
   }
#endif

   png_init_io(png_ptr, fp);


   /* write header */
#if USE_SETJMP
   if( setjmp(png_jmpbuf(png_ptr)) )
   {
      fail( "[pngwrite] Error during writing header\n" );
   }
#endif

   png_set_IHDR( png_ptr, info_ptr, image->width, height,
                 1, PNG_COLOR_TYPE_PALETTE, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE );

   png_set_PLTE( png_ptr, info_ptr, palette_p, 2 );
   png_write_info( png_ptr, info_ptr );


   /* write bytes */
#if USE_SETJMP
   if( setjmp(png_jmpbuf(png_ptr)) )
   {
      fail( "[pngwrite] Error during writing bytes\n" );
   }
#endif

   if( height < image->height )
   {
      firstrowps += (image->height - height) / 2;
      png_write_rows( png_ptr, firstrowps, height );
   }
   else
   {
      png_write_image( png_ptr, image->rowps );
   }


   /* end write */
#if USE_SETJMP
   if( setjmp(png_jmpbuf(png_ptr)) )
   {
      fail("[pngwrite] Error during end of write\n");
   }
#endif

   png_write_end( png_ptr, NULL );
   png_destroy_write_struct( &png_ptr, &info_ptr );

   fclose(fp);
}
